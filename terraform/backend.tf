terraform {
  backend "s3" {
    bucket = "terraforms3backendprojeto"
    key = "terraform.tfstate"
    region = "us-east-2"
    dynamodb_table = "terraform-lock"
  }
}

