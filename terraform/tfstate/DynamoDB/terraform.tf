provider "aws" {
  region     = ""
  access_key = ""
  secret_key = ""
  version = "~> 2.0"
}

resource "aws_dynamodb_table" "terraform_state_lock" {
  name           = "terraform-lock"
  read_capacity  = 10
  write_capacity = 10
  hash_key       = "LockID"
attribute {
    name = "LockID"
    type = "S"
  }
}

