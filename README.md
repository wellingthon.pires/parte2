# Deploy EKS via Terraform

Variáveis do Projeto
====================

- `AWS_ACCESS_KEY`: Access Key gerado via IAM;
- `AWS_SECRET_KEY`: Secret Key gerado via IAM;
- `AWS_REGION`: Região Padrão do Usuário;
- `TERRAFORM_VERSION`: Versão Terraform utilizada no build da imagem;

GITLAB-CI
=========

## Steps do Pipeline

O `.gitlab-ci.yml` do projeto implementa os seguintes passos no pipeline, em ordem. 

 1. Validate
 2. Plan
 3. Deploy 
 4. Destroy


JOBs
====


## Validate

Responsável pela validação do código do terraform.

## Plan

Executa o terraform plan e salva em um arquivo em forma de artefato.

## Deploy

Job responsável por criar a nossa infraestrutura de forma automatizada.

## Destroy

Job responsável por realizar o destroy de toda a infraestrutura criada via terraform.

## Melhores Pratricas

O tfstate do terraform esta armazenado no AWS S3 e uma tabela no DynamoDB ele é responsável por realizar o lock do tfstate durante a execução de um job.
